﻿Select
    pizza365_db.customers.customerNumber,
    pizza365_db.customers.customerName
From
    pizza365_db.orderdetails Inner Join
    pizza365_db.orders On pizza365_db.orderdetails.orderNumber = pizza365_db.orders.orderNumber Inner Join
    pizza365_db.customers On pizza365_db.orders.customerNumber = pizza365_db.customers.customerNumber
Group By
    pizza365_db.customers.customerNumber,
    pizza365_db.customers.customerName
