﻿Select
    pizza365_db.products.productCode,
    pizza365_db.products.productName,
    Sum(pizza365_db.orderdetails.quantityOrdered * pizza365_db.orderdetails.priceEach) As Total
From
    pizza365_db.orderdetails Inner Join
    pizza365_db.products On pizza365_db.orderdetails.productCode = pizza365_db.products.productCode
Group By
    pizza365_db.products.productCode,
    pizza365_db.products.productName
Having
    Sum(pizza365_db.orderdetails.quantityOrdered * pizza365_db.orderdetails.priceEach) >= 500
