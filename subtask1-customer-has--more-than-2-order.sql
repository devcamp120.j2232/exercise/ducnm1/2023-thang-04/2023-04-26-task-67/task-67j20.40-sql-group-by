﻿Select
    Count(pizza365_db.orders.orderNumber) As Count_orderNumber,
    pizza365_db.orders.customerNumber,
    pizza365_db.customers.customerName
From
    pizza365_db.orders Inner Join
    pizza365_db.customers On pizza365_db.orders.customerNumber = pizza365_db.customers.customerNumber
Group By
    pizza365_db.orders.customerNumber,
    pizza365_db.customers.customerName
Having
    Count(pizza365_db.orders.orderNumber) >= 2
Order By
    Count_orderNumber,
    pizza365_db.orders.customerNumber
