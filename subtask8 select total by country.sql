﻿Select
    pizza365_db.customers.country,
    Sum(pizza365_db.orderdetails.quantityOrdered * pizza365_db.orderdetails.priceEach) As Toal
From
    pizza365_db.customers Inner Join
    pizza365_db.orders On pizza365_db.orders.customerNumber = pizza365_db.customers.customerNumber Inner Join
    pizza365_db.orderdetails On pizza365_db.orderdetails.orderNumber = pizza365_db.orders.orderNumber
Group By
    pizza365_db.customers.country
