﻿Select
    pizza365_db.orders.orderNumber,
    pizza365_db.orders.orderDate,
    Week(pizza365_db.orders.orderDate) As Week,
    Sum(pizza365_db.orderdetails.quantityOrdered * pizza365_db.orderdetails.priceEach) As Total
From
    pizza365_db.orders Inner Join
    pizza365_db.orderdetails On pizza365_db.orderdetails.orderNumber = pizza365_db.orders.orderNumber
Group By
    pizza365_db.orders.orderNumber,
    pizza365_db.orders.orderDate
