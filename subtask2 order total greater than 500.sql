﻿Select
    pizza365_db.orders.orderNumber,
    Sum(pizza365_db.orderdetails.quantityOrdered * pizza365_db.orderdetails.priceEach) As Total
From
    pizza365_db.orders Inner Join
    pizza365_db.orderdetails On pizza365_db.orderdetails.orderNumber = pizza365_db.orders.orderNumber
Group By
    pizza365_db.orders.orderNumber
Having
    Sum(pizza365_db.orderdetails.quantityOrdered * pizza365_db.orderdetails.priceEach) >= 500
Order By
    pizza365_db.orders.orderNumber
