﻿Select
    pizza365_db.orders.orderNumber,
    pizza365_db.orders.orderDate,
    Week(pizza365_db.orders.orderDate) As Week
From
    pizza365_db.orders
